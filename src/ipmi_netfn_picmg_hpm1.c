/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file ipmi_netfn_picmg_hpm1.c
 *
 * @author Andre Muller Cascadan
 *
 * @brief  Response functions for HPM.1 commands.
 *
 * This module implements the IPMI commands specified in the HPM.1 Hardware
 * Platform Management IPM Controller Firmware Upgrade Specification, which is
 * part of the PICMG Network Function.
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "FreeRTOS.h"
#include "queue.h"
#include "semphr.h"

#include "hpm1_ctrls.h"
#include "ipmc_ios.h"

#define PICMG_IDENTIFIER 0x00
#define HPM1_VERSION     0x00

// Special completion codes
#define COMPLETION_CODE_UPGRADE_NOT_SUPPORTED 0x81
#define COMPLETION_CODE_INVALID_COMPONENT_ID  0x82
#define COMPLETION_CODE_INVALID_PROPERTY      0x83

#define COMPLETION_CODE_LONG_DURATION_CMD_IN_PROGRESS 0x80
#define COMPLETION_CODE_ROLLBACK_IN_PROGRESS          0x80

#define CMD_CODE_INITIATE_UPGRADE_ACTION  0x31
#define CMD_CODE_UPLOAD_FIRMWARE_BLOCK    0x32
#define CMD_CODE_FINISH_FIRMWARE_UPLOAD   0x33
#define CMD_CODE_ACTIVATE_FIRMWARE        0x35
#define CMD_CODE_INITIATE_MANUAL_ROLLBACK 0x38

// Maximum amount of image bytes which can be transferred by each Upload Command issuing.
#define MAX_BLOCK_SIZE 23


typedef enum
{
	hpm1_cmd_initiate_backup,
	hpm1_cmd_initiate_prepare,
	hpm1_cmd_initiate_upload_for_upgrade,
	hpm1_cmd_initiate_upload_for_compare,
	hpm1_cmd_upload,
	hpm1_cmd_upload_finish,
	hpm1_cmd_activate,
	hpm1_cmd_manual_rollback

}upgrade_command_name_t;

typedef struct
{
	upgrade_command_name_t name;
	uint8_t component_mask;
	uint8_t block_data[MAX_BLOCK_SIZE];
	uint8_t block_size;

}upgrade_command_t;

typedef struct
{
    bool     long_duration_cmd_in_progress;
    uint8_t  component_mask;
    uint8_t  cmd_code;
    uint8_t  completion_code;
    uint32_t total_uploaded_bytes;
    uint8_t  block_counter;
    
}upgrade_status_t;


uint8_t hpm1_global_capabilities = UPGRADE_UNDESIRABLE; // Refuses update by default
hpm1_timeouts_t hpm1_timeouts = {0x00, 0x00, 0x00, 0x00};
hpm1_component_properties_t* hpm1_component_properties[8] = {NULL};

static uint8_t components_present = 0;
static upgrade_status_t upgrade_status = {false, 0, 0, 0, 0};
static bool hpm1_is_ready = false;

// Queue for controlling the Upgrade Task flow
static QueueHandle_t upgrade_command_queue;
static uint8_t       upgrade_command_queue_storage[sizeof(upgrade_command_t)];
static StaticQueue_t upgrade_command_queue_buffer;

// Mutex for protecting status
static SemaphoreHandle_t upgrade_status_mutex = NULL;
static StaticSemaphore_t upgrade_status_mutex_buffer;

TaskHandle_t hpm1_upgrade_task_handle = NULL;


void ipmi_cmd_get_target_upgrade_capabilities (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code)
{
        response_bytes[0] = PICMG_IDENTIFIER;
        response_bytes[1] = HPM1_VERSION;
        response_bytes[2] = hpm1_global_capabilities;
        response_bytes[3] = hpm1_timeouts.upgrade_timeout;
        response_bytes[4] = hpm1_timeouts.selftest_timeout;
        response_bytes[5] = hpm1_timeouts.rollback_timeout;
        response_bytes[6] = hpm1_timeouts.inaccessibility_timeout;
        response_bytes[7] = components_present;
        
        *completion_code = 0;
        *res_len = 8;
}



void ipmi_cmd_get_component_properties (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code)
{
	uint8_t component_number = request_bytes[1];

	// Check if component is present
	if( (component_number > 7) || !((1 << component_number) & components_present) )
	{
		*completion_code = COMPLETION_CODE_INVALID_COMPONENT_ID;
		*res_len = 0;
		return;
	}

	const hpm1_component_properties_t* comp_prop   = hpm1_component_properties[component_number];
	uint8_t property_selector = request_bytes[2];

	response_bytes[0] = PICMG_IDENTIFIER;

	switch( property_selector )
	{
		case 0: // General Component Properties
			response_bytes[1] = (1<<2); //Preparation is always supported
			if( comp_prop->payload_cold_reset_is_required )
				response_bytes[1] |= (1<<5);
			if( comp_prop->deferred_activation_is_supported )
				response_bytes[1] |= (1<<4);
			if( comp_prop->firmware_comparison_is_supported )
				response_bytes[1] |= (1<<3);
			if( comp_prop->rollback_is_supported )
			{
				if(comp_prop->backup_cmd_is_required)
					response_bytes[1] |= (1<<0);
				else
					response_bytes[1] |= (1<<1);
			}
			*completion_code = 0;
			*res_len = 2;
			break;

		case 1: // Current version
			response_bytes[1] = comp_prop->current_firmware_revision_major & 0x7F;
			response_bytes[2] = comp_prop->current_firmware_revision_minor;
			response_bytes[3] = comp_prop->current_firmware_revision_aux[0];
			response_bytes[4] = comp_prop->current_firmware_revision_aux[1];
			response_bytes[5] = comp_prop->current_firmware_revision_aux[2];
			response_bytes[6] = comp_prop->current_firmware_revision_aux[3];
			*completion_code = 0;
			*res_len = 7;
			break;

		case 2: // Description string
			strncpy( (char*)(response_bytes+1), comp_prop->description_string, 12 );
			if( strlen( comp_prop->description_string ) > 11 ) // Safety truncation
				response_bytes[12] = 0;
			*completion_code = 0;
			*res_len = 2 + strlen( (char*)(response_bytes+1) );
			break;

        case 3: // Rollback firmware version
            if( comp_prop->rollback_is_supported )
			{
				if( comp_prop->backup_is_available )
				{
					response_bytes[1] = comp_prop->rollback_firmware_revision_major & 0x7F;
					response_bytes[2] = comp_prop->rollback_firmware_revision_minor;
					response_bytes[3] = comp_prop->rollback_firmware_revision_aux[0];
					response_bytes[4] = comp_prop->rollback_firmware_revision_aux[1];
					response_bytes[5] = comp_prop->rollback_firmware_revision_aux[2];
					response_bytes[6] = comp_prop->rollback_firmware_revision_aux[3];
				}
				else
				{
					response_bytes[1] = 0x7F;
					response_bytes[2] = 0xFF;
					response_bytes[3] = response_bytes[4] = response_bytes[5] = response_bytes[6] = 0;
				}
				*completion_code = 0;
				*res_len = 7;
			}
			else
			{
				*completion_code = COMPLETION_CODE_INVALID_PROPERTY;
				*res_len = 0;
			}
            break;

        case 4: // Deferred upgrade firmware version
        	if( comp_prop->deferred_activation_is_supported )
        	{
        		if( comp_prop->deferred_is_pending_activation )
        		{
        			response_bytes[1] = comp_prop->deferred_firmware_revision_major & 0x7F;
        			response_bytes[2] = comp_prop->deferred_firmware_revision_minor;
        			response_bytes[3] = comp_prop->deferred_firmware_revision_aux[0];
        			response_bytes[4] = comp_prop->deferred_firmware_revision_aux[1];
        			response_bytes[5] = comp_prop->deferred_firmware_revision_aux[2];
        			response_bytes[6] = comp_prop->deferred_firmware_revision_aux[3];
        		}
        		else
        		{
        			response_bytes[1] = 0x7F;
        			response_bytes[2] = 0xFF;
        			response_bytes[3] = response_bytes[4] = response_bytes[5] = response_bytes[6] = 0;
        		}
        		*completion_code = 0;
        		*res_len = 7;
        	}
        	else
        	{
        		*completion_code = COMPLETION_CODE_INVALID_PROPERTY;
        		*res_len = 0;
        	}
        	break;

		default: // Unsupported Property
			*completion_code = COMPLETION_CODE_INVALID_PROPERTY;
			*res_len = 0;
	}
}




void ipmi_cmd_abort_firmware_upgrade (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code)
{
}

void ipmi_cmd_initiate_upgrade_action (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code)
{
	while( !hpm1_is_ready ) vTaskDelay( pdMS_TO_TICKS(50) );

	upgrade_command_t command;
	const uint8_t     component_mask = request_bytes[1];
	
	if( component_mask == 0 )
	{
		*completion_code = COMPLETION_CODE_INVALID_COMPONENT_ID;
		*res_len = 0;
		return;
	}
	
	// Long-duration command in course
	xSemaphoreTake( upgrade_status_mutex, pdMS_TO_TICKS(1000) );
	const bool long_duration_cmd_in_progress = upgrade_status.long_duration_cmd_in_progress;
	xSemaphoreGive( upgrade_status_mutex );
	if( long_duration_cmd_in_progress )
	{
		*completion_code = COMPLETION_CODE_LONG_DURATION_CMD_IN_PROGRESS;
		*res_len = 0;
		return;
	}

	xSemaphoreTake( upgrade_status_mutex, pdMS_TO_TICKS(1000) );
	upgrade_status.long_duration_cmd_in_progress = true;
	upgrade_status.component_mask = component_mask;
	upgrade_status.cmd_code = CMD_CODE_INITIATE_UPGRADE_ACTION;
	upgrade_status.completion_code = COMPLETION_CODE_LONG_DURATION_CMD_IN_PROGRESS;
	upgrade_status.total_uploaded_bytes = 0;
	upgrade_status.block_counter = 0;
	xSemaphoreGive( upgrade_status_mutex );

	uint8_t upgrade_action = request_bytes[2];
	command.component_mask = component_mask;
	
	switch( upgrade_action )
	{
		case 0x00: // Backup component
			command.name = hpm1_cmd_initiate_backup;
			break;
		case 0x01: // Prepare component
			command.name = hpm1_cmd_initiate_prepare;
			break;
		case 0x02: // Upload for upgrade
			command.name = hpm1_cmd_initiate_upload_for_upgrade;
			break;
		case 0x03: // Upload for compare
			command.name = hpm1_cmd_initiate_upload_for_compare;
			break;
	}

	// Send command to be executed
	xQueueSendToBack( upgrade_command_queue, &command, 0);

	response_bytes[0] = PICMG_IDENTIFIER;
	*completion_code = COMPLETION_CODE_LONG_DURATION_CMD_IN_PROGRESS;
	*res_len = 1;
}

void ipmi_cmd_upload_firmware_block (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code)
{
	while( !hpm1_is_ready ) vTaskDelay( pdMS_TO_TICKS(50) );

	// Long-duration command in course
	xSemaphoreTake( upgrade_status_mutex, pdMS_TO_TICKS(1000) );
	const bool long_duration_cmd_in_progress = upgrade_status.long_duration_cmd_in_progress;
	const uint8_t expected_block = upgrade_status.block_counter;
	xSemaphoreGive( upgrade_status_mutex );
	if( long_duration_cmd_in_progress )
	{
		*completion_code = COMPLETION_CODE_LONG_DURATION_CMD_IN_PROGRESS;
		*res_len = 0;
		return;
	}

	upgrade_command_t command;
	const uint8_t     block_size = req_len - 2;

	if(request_bytes[1] == expected_block)
	{
		// Compose the command to be executed by the upgrade task
		xSemaphoreTake( upgrade_status_mutex, pdMS_TO_TICKS(1000) );
		command.name           = hpm1_cmd_upload;
		command.component_mask = upgrade_status.component_mask; // Component which is being upgraded is always informed to HAL
		command.block_size     = block_size;
		memcpy( command.block_data, &request_bytes[2], block_size );

		// Update status
		upgrade_status.long_duration_cmd_in_progress = true;
		upgrade_status.cmd_code = CMD_CODE_UPLOAD_FIRMWARE_BLOCK;
		upgrade_status.completion_code = COMPLETION_CODE_LONG_DURATION_CMD_IN_PROGRESS;
		upgrade_status.total_uploaded_bytes += block_size;
		upgrade_status.block_counter = expected_block + 1;
		xSemaphoreGive( upgrade_status_mutex );

		// Send command to be executed
		xQueueSendToBack( upgrade_command_queue, &command, 0);

		response_bytes[0] = PICMG_IDENTIFIER;
		*completion_code = COMPLETION_CODE_LONG_DURATION_CMD_IN_PROGRESS;
		*res_len = 1;
	}
	else
	{
		response_bytes[0] = PICMG_IDENTIFIER;
		*completion_code = 0;
		*res_len = 1;
	}
}

void ipmi_cmd_finish_firmware_upload (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code)
{
	while( !hpm1_is_ready ) vTaskDelay( pdMS_TO_TICKS(50) );

	xSemaphoreTake( upgrade_status_mutex, pdMS_TO_TICKS(1000) );
	const bool     long_duration_cmd_in_progress = upgrade_status.long_duration_cmd_in_progress;
	const uint32_t total_uploaded_bytes          = upgrade_status.total_uploaded_bytes;
	xSemaphoreGive( upgrade_status_mutex );

	// Long-duration command in course
	if( long_duration_cmd_in_progress )
	{
		*completion_code = COMPLETION_CODE_LONG_DURATION_CMD_IN_PROGRESS;
		*res_len = 0;
		return;
	}

	// Check if the amount of received bytes matches with the expected
	const uint32_t expected_received_bytes = ( (uint32_t)request_bytes[5] << 24) +
	                                         ( (uint32_t)request_bytes[4] << 16) +
	                                         ( (uint32_t)request_bytes[3] <<  8) +
	                                         ( (uint32_t)request_bytes[2]      );
	if( expected_received_bytes != total_uploaded_bytes )
	{
		*completion_code = 0x81;  // Special completions for this command: Mismatch between the expected and received.
		*res_len = 0;
		return;
	}

	// Compose the command to be executed by the upgrade task
	upgrade_command_t command;
	command.name           = hpm1_cmd_upload_finish;
	command.component_mask = 1<<request_bytes[1];

	// Update status
	xSemaphoreTake( upgrade_status_mutex, pdMS_TO_TICKS(1000) );
	upgrade_status.long_duration_cmd_in_progress = true;
	upgrade_status.cmd_code = CMD_CODE_FINISH_FIRMWARE_UPLOAD;
	upgrade_status.completion_code = COMPLETION_CODE_LONG_DURATION_CMD_IN_PROGRESS;
	xSemaphoreGive( upgrade_status_mutex );

	// Send command to be executed
	xQueueSendToBack( upgrade_command_queue, &command, 0);

	response_bytes[0] = PICMG_IDENTIFIER;
	*completion_code = COMPLETION_CODE_LONG_DURATION_CMD_IN_PROGRESS;
	*res_len = 1;
}

void ipmi_cmd_get_upgrade_status (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code)
{
	while( !hpm1_is_ready ) vTaskDelay( pdMS_TO_TICKS(50) );

	xSemaphoreTake( upgrade_status_mutex, pdMS_TO_TICKS(1000) );
	response_bytes[0] = PICMG_IDENTIFIER;
	response_bytes[1] = upgrade_status.cmd_code;
	response_bytes[2] = upgrade_status.completion_code;
	xSemaphoreGive( upgrade_status_mutex );

	*completion_code = 0x00;
	*res_len = 3;
}

void ipmi_cmd_activate_firmware (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code)
{
	while( !hpm1_is_ready ) vTaskDelay( pdMS_TO_TICKS(50) );

	// Long-duration command in course
	xSemaphoreTake( upgrade_status_mutex, pdMS_TO_TICKS(1000) );
	const bool long_duration_cmd_in_progress = upgrade_status.long_duration_cmd_in_progress;
	xSemaphoreGive( upgrade_status_mutex );
	if( long_duration_cmd_in_progress )
	{
		*completion_code = COMPLETION_CODE_LONG_DURATION_CMD_IN_PROGRESS;
		*res_len = 0;
		return;
	}

	// Compose the command to be executed by the upgrade task
	upgrade_command_t command;
	command.name = hpm1_cmd_activate;

	// Update status
	xSemaphoreTake( upgrade_status_mutex, pdMS_TO_TICKS(1000) );
	upgrade_status.long_duration_cmd_in_progress = true;
	upgrade_status.cmd_code = CMD_CODE_ACTIVATE_FIRMWARE;
	upgrade_status.completion_code = COMPLETION_CODE_LONG_DURATION_CMD_IN_PROGRESS;
	xSemaphoreGive( upgrade_status_mutex );

	// Send command to be executed
	xQueueSendToBack( upgrade_command_queue, &command, 0);

	response_bytes[0] = PICMG_IDENTIFIER;
	*completion_code = COMPLETION_CODE_LONG_DURATION_CMD_IN_PROGRESS;
	*res_len = 1;

}

void ipmi_cmd_query_selftest_results (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code)
{
	//ipmc_ios_printf("ipmi_cmd_query_selftest_results TODO\r\n");
}

void ipmi_cmd_query_rollback_status (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code)
{
	// Simplification here! Just tell agent that rollback is complete. TODO: Implement rollback completion check

	response_bytes[0] = PICMG_IDENTIFIER;
	response_bytes[1] = 0xFF;
	*completion_code  = 0;
	*res_len = 2;
}

void ipmi_cmd_initiate_manual_rollback (uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code)
{
	while( !hpm1_is_ready ) vTaskDelay( pdMS_TO_TICKS(50) );

	// Long-duration command in course
	xSemaphoreTake( upgrade_status_mutex, pdMS_TO_TICKS(1000) );
	const bool long_duration_cmd_in_progress = upgrade_status.long_duration_cmd_in_progress;
	xSemaphoreGive( upgrade_status_mutex );
	if( long_duration_cmd_in_progress )
	{
		*completion_code = COMPLETION_CODE_LONG_DURATION_CMD_IN_PROGRESS;
		*res_len = 0;
		return;
	}

	// Compose the command to be executed by the upgrade task
	upgrade_command_t command;
	command.name = hpm1_cmd_manual_rollback;

	// Update status
	xSemaphoreTake( upgrade_status_mutex, pdMS_TO_TICKS(1000) );
	upgrade_status.long_duration_cmd_in_progress = true;
	upgrade_status.cmd_code = CMD_CODE_INITIATE_MANUAL_ROLLBACK;
	upgrade_status.completion_code = COMPLETION_CODE_ROLLBACK_IN_PROGRESS;
	xSemaphoreGive( upgrade_status_mutex );

	// Send command to be executed
	xQueueSendToBack( upgrade_command_queue, &command, 0);

	response_bytes[0] = PICMG_IDENTIFIER;
	*completion_code = COMPLETION_CODE_ROLLBACK_IN_PROGRESS;
	*res_len = 1;
}




hpm1_component_properties_t* hpm1_add_component( uint8_t component_number )
{

	if( component_number > 7 ) // If invalid component number
		return NULL;

	if( (1 << component_number) & components_present) // If already present
		return NULL;

	components_present |= (1 << component_number); // Mark as present

	hpm1_component_properties[component_number] = pvPortMalloc( sizeof( hpm1_component_properties_t ) );

	return hpm1_component_properties[component_number];
}




void hpm1_upgrade_task( void *pvParameters )
{
	upgrade_command_t command;
	uint8_t           component_number;
	int               cb_return;
	uint8_t           completion_code;

	upgrade_command_queue = xQueueCreateStatic( 1,
	                                            sizeof(upgrade_command_t),
	                                            upgrade_command_queue_storage,
	                                            &upgrade_command_queue_buffer  );


	upgrade_status_mutex = xSemaphoreCreateMutexStatic( &upgrade_status_mutex_buffer );

	hpm1_is_ready = true;

	while(1)
	{
		// Wait for a command
		xQueueReceive( upgrade_command_queue, &command, portMAX_DELAY );

		/*
		 * Get the component number from component mask. It gets the first "1" bit found in the mask.
		 * Component number is only used in cases the component mask informs only one component.
		 */
		uint8_t temp_mask = command.component_mask;
		for( int i=0; i<8; i++ )
		{
			if( (temp_mask & 0x01) == 0x01 )
			{
				component_number = i;
				break;
			}
			else
				temp_mask /= 2; // right shift
		}

		completion_code = 0;

		switch( command.name )
		{
			case hpm1_cmd_initiate_backup:
				hpm1_cmd_initiate_backup_cb( command.component_mask );
				break;
			case hpm1_cmd_initiate_prepare:
				hpm1_cmd_initiate_prepare_cb( command.component_mask );
				break;
			case hpm1_cmd_initiate_upload_for_upgrade:
				hpm1_cmd_initiate_upload_for_upgrade_cb( component_number );
				break;
			case hpm1_cmd_initiate_upload_for_compare:
				hpm1_cmd_initiate_upload_for_compare_cb( component_number );
				break;
			case hpm1_cmd_upload:
				hpm1_cmd_upload_cb( component_number, command.block_data, command.block_size);
				break;
			case hpm1_cmd_upload_finish:
				cb_return = hpm1_cmd_upload_finish_cb( component_number );
				if( cb_return == HPM1_CB_RETURN_CHECKSUM_ERROR )
					completion_code = 0x82;
				break;
			case hpm1_cmd_activate:
				hpm1_cmd_activate_cb();
				break;
			case hpm1_cmd_manual_rollback:
				cb_return = hpm1_cmd_manual_rollback_cb();
				if( cb_return == HPM1_CB_RETURN_ROLLBACK_ERROR )
					completion_code = 0x81; // Rollback failure
				break;
		}

		// When the callback returns, update the status
		xSemaphoreTake( upgrade_status_mutex, pdMS_TO_TICKS(1000) );
		upgrade_status.long_duration_cmd_in_progress = false; // Long-duration command concludes.
		upgrade_status.completion_code = completion_code;
		xSemaphoreGive( upgrade_status_mutex );
	}
}








