/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file handle_switch.c
 * 
 * @authors Alison Franca Queiroz da Costa
 * @authors Andre Muller Cascadan
 * 
 * @brief  Task to sample and process the Handle Switch.
 */


/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"


#include "ipmc_ios.h"
#include "fru_state_machine.h"


#define SAMPLING_PERIOD 500 //milliseconds


TaskHandle_t ipmc_handle_switch_task_handle = NULL;

/*
 * Read the state transition of the ATCA handle
 * 
 * Return to the queue_fru_transitions the change state of the ATCA handle.
 * If the handle change from OPEN to CLOSE then CLOSE_HANDLE will be sent.
 * If the handle change form CLOSE to OPEN then OPEN_HANDLE will be sent.
 * 
 */ 
void ipmc_handle_switch_task( void *pvParameters )
{
  int handle_prev_state;
  int handle_curr_state;
  fru_transition_t transition;
  int debounce = 0;
  
  // Wait for resources
  while ( ipmc_ios_ready() != pdTRUE )
    vTaskDelay( pdMS_TO_TICKS(100) );

  while ( queue_fru_transitions == NULL )
    vTaskDelay( pdMS_TO_TICKS(100) );


  // Initialization of the handle states
  handle_curr_state = ipmc_ios_read_handle();
  handle_prev_state = handle_curr_state;
  
  for( ;; )
  {
  
    handle_prev_state = handle_curr_state;
    handle_curr_state = ipmc_ios_read_handle();
    
    // Debounce "0": waiting for transition in the handle
    if(debounce == 0 )
    {  
      
      if(handle_prev_state != handle_curr_state)
      {
        
        if(handle_curr_state == 0)
          transition = CLOSE_HANDLE;
        else
          transition = OPEN_HANDLE;
        
        debounce ++; // start debounce count
      }
      
    }
    else if((debounce >= 1) && (debounce <= 3))
    {
      
      if(handle_prev_state == handle_curr_state)
        debounce ++;  // Stay counting if the reading doesn't change
      else
        debounce = 0;
      
    }
    else if(debounce == 4)
    {
      
      debounce = 0; // Debounce finishes.
      xQueueSendToBack(queue_fru_transitions, &transition, 0UL);
      
    }
    
    // Delay is shorter when filtering (debounce counting)
    if(debounce > 0)
      vTaskDelay( pdMS_TO_TICKS(SAMPLING_PERIOD) / 5 );
    else
      vTaskDelay( pdMS_TO_TICKS(SAMPLING_PERIOD) );
  
  }
}
