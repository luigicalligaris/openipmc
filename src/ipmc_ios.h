/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file ipmc_ios.h
 * 
 * @author Andre Muller Cascadan
 * 
 * @brief Hardware-independent interface for the IPMC IOs.
 * 
 * This header file contains function declarations to access the IOs of the
 * IPMC in a hardware-independent way. It defines an abstraction to keep the
 * OpenIPMC code independent on the hardware particularities.
 * 
 * The implementation of this functions, in the other hand, is 
 * hardware-dependent, which means that it must be adapted to the chosen
 * platform by the designer.
 */


#ifndef IPMC_IOS_H
#define IPMC_IOS_H

#include <stdint.h>


/**
 * Value returned by {@link ipmc_ios_read_haddress()} if the parity
 * check fails.
 */
#define HA_PARITY_FAIL 0x80


/// @name Handle Switch state
/// @{
#define HANDLE_SWITCH_IS_OPEN   1
#define HANDLE_SWITCH_IS_CLOSED 0
/// @}



/// @name "IPMB Send" status
/// @{
#define IPMB_SEND_DONE 1
#define IPMB_SEND_FAIL 0
/// @}



/**
 * @name Peripheral Initialization
 * @{
 */

/**
 * @brief Check if peripherals are ready.
 * 
 * This function is used by OpenIPMC to check if the peripherals are ready to
 * be used.
 * 
 * OpenIPMC polls this function several times during its initialization process.
 * 
 * @return It is expected to return FALSE from the moment the system starts and
 * and during the peripheral initialization steps, which depends on the platform.
 * TRUE if all the functions declared in this file are ready to be used by
 * OpenIPMC, in other words, if the "physical layer" is already initialized.
 */
_Bool ipmc_ios_ready(void);

///@}



/**
 * Check the state of the Handle Switch
 * 
 * @return HANDLE_SWITCH_IS_OPEN or HANDLE_SWITCH_IS_CLOSED
 */
int  ipmc_ios_read_handle(void);

/**
 * Read the Hardware Address from the backplane and check the parity
 * 
 * @return Return the 7-bit Hardware Address or {@link HA_PARITY_FAIL} 
 * if parity check fails.
 */
uint8_t   ipmc_ios_read_haddress(void);


/**
 * @name IPMB functions
 * @{
 * 
 * These functions are called by OpenIPMC to operate on the on IPMB channels.
 */

/**
 * @brief Set the IPMB address
 * 
 * Set the address of both I2C channels when acting as slave (listening for messages)
 */
void ipmc_ios_ipmb_set_addr(uint8_t);

/** 
 * @brief Send IPMI message on IPMB-A channel
 * 
 * Called by OpenIPMC to send an IPMI message trough IPMB-A.
 * 
 * The IPMI message is an array of bytes to be sent via I2C in one single "master-write"
 * operation, headed by the slave I2C address and ended by the STOP. The amount of 
 * bytes is variable and maximum of 32 bytes (counting the I2C address byte).
 * 
 * This function blocks until the transmission finishes or fails.
 * 
 * @param ipmi_msg Array containing the bytes to be transmitted. The first byte contains
 * the I2C address of the destination slave in 8bit format (with R/W bit as '0').
 * 
 * @param byte_count Number of bytes in ipmi_msg (counting the I2C address byte).
 * 
 * @return IPMB_SEND_DONE if message is successfully sent or IPMB_SEND_FAIL in case
 * of failure (bus busy or arbitration loss).
 */
int ipmc_ios_ipmba_send(uint8_t ipmi_msg[], int byte_count);
int ipmc_ios_ipmbb_send(uint8_t ipmi_msg[], int byte_count); ///< The same as {@link ipmc_ios_ipmba_send()} for IPMB-B

/** 
 * @brief Read IPMI message from IPMB-A channel
 * 
 * Called by OpenIPMC to read an IPMI message received in IPMB-A.
 * 
 * IPMI messages are received with the I2C bus in slave mode, listening in the 
 * address set by {@link ipmc_ios_ipmb_set_addr()}.
 * 
 * When a message is received from the bus, it must be buffered, and the
 * interface must unblock {@link ipmc_ios_ipmb_wait_input_msg()}, which causes
 * OpenIPMC to "pop" the buffer by calling this function.
 * 
 * @param ipmi_msg Pointer to the array where the message must be copied to.
 * The first byte is the I2C address in the header of the incoming bytes in 
 * 8bit format (with R/W bit as '0'). Since most I2C peripherals do not include the 
 * address header with the received data, it need to be manually included here.
 * 
 * @return The number of bytes copied to ipmi_msg (counting the I2C address byte).
 * ZERO must be returned in case of buffer is empty.
 */
int ipmc_ios_ipmba_read(uint8_t ipmi_msg[]);
int ipmc_ios_ipmbb_read(uint8_t ipmi_msg[]); ///< The same as {@link ipmc_ios_ipmba_read()} for IPMB-B

/**
 * Called by OpenIPMC when waiting for input messages.
 * 
 * This function should blocks until a message is ready to be read in the
 * IPMB-A or IPMB-B input buffers.
 * 
 * @b Tip: This function should behave like a "Take Semaphore", so a semaphore
 * could be used to implement it. This semaphore should be given by the I2C interfaces
 * when they finish the receiving of an I2C packet, and taken inside this function.
 */
void ipmc_ios_ipmb_wait_input_msg(void);

/**
 * @}
 */

/**
 * @brief Controls the sate of the Blue LED.
 * 
 * This function is called by OpenIPMC to set the Blue LED ON or OFF.
 * 
 * @param blue_led_state 1 to turn-on the LED. 0 to turn-off the LED.
 * 
 */
void ipmc_ios_blue_led(int blue_led_state);

/**
 * @brief Custom printf used by OpenIPMC to print its output
 * 
 * An implementation of printf must be provided by the platform
 */
void ipmc_ios_printf(const char* format, ...);

#endif // IPMC_IOS_H

