/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file ipmi_netfn_picmg.c
 *
 * @authors Alison Franca Queiroz da Costa
 * @authors Andre Muller Cascadan
 * @authors Bruno Augusto Casu
 * 
 * @brief  Response functions for PICMG Network Function commands.
 *
 * This file contains the specific functions to manage the requests of Sensor and Event Network Function commands (definition in PICMG v3.0 and IPMI v1.5).
 * Each function returns the response data bytes and the completion code. PICMG commands are the most significant in the control of ATCA based systems.
 * They are responsible for power control and management of the Hot Swap function. All the specification of the commands are defined in PICMG v3.0.
 *
 * The specific functions are called by ipmi_msg_solve_request_ipmb0().
 */

#include <stdint.h>

#include "ipmb_0.h"
#include "fru_state_machine.h"
#include "power_manager.h"
#include "sdr_manager.h"
#include "ipmc_ios.h"

/**
 * @}
 * @name Shelf Address control variables
 * 
 * These variables are used in the ipmi_cmd_get_shelf_address_info() and ipmi_cmd_set_shelf_address_info() to manage
 * the Shelf Address, configured via Shelf Manager Interface
 *
 * The Shelf Address Type starts with value 0x00 (unconfigured) 
 */
uint8_t shelf_address = 0x00;
uint8_t shelf_address_type = 0x00;
///@}

/**
 * @{
 * @name PICMG NetFn commands
 */

/**
 * @brief Specific function to provide a response for "Get PICMG Properties" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.                        
 * @param response_bytes  Data bytes returned by the this function (definition in PICMG v3.0). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_get_picmg_prop( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    if(request_bytes[0] == 0x00) // Check PICMG Identifier
    {
        response_bytes[0] = 0x00; // PICMG Identifier
        
        response_bytes[1] = 0x32; // PICMG Extension version 2.3 (bits 7:4 holds the Least Significant digit, bits 3:0 holds the Most Significant digit)
        
        response_bytes[2] = 0x00; // Max FRU Device ID
        
        response_bytes[3] = 0x00; // FRU Device ID for IPMC

        *res_len = 4;
        *completion_code = 0; // OK
    }

    else
    {
        *res_len = 0;
        *completion_code = 0xCC; // Invalid data field in Request
    }
}

/**
 * @brief Specific function to provide a response for "Get Device Locator Record ID" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.                        
 * @param response_bytes  Data bytes returned by the this function (definition in PICMG v3.0). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 * 
 * This Function provides to Shelf Manager the IPMC Record ID.
 */
void ipmi_cmd_get_device_loc_record_id( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    if(request_bytes[0] == 0x00) // Check PICMG Identifier
    {
        response_bytes[0] = 0x00; // PICMG Identifier
        int l = 0;
        uint16_t aux = 0;

        while (l==0) 
        {
            if (allrecords[aux].type != MANAGEMENT_CONTROLLER_DEVICE_LOCATOR) // Search for type 12h record
            {
            	aux++; 
            }
            else l=1;
        }
        // aux = number of the record with the IPMC information
        response_bytes[1] = (uint8_t) ((aux & 0x00FF) >> 0); // Record ID for the Device Locator SDR (Least Significant Byte - LS)
        response_bytes[2] = (uint8_t) ((aux & 0xFF00) >> 8); // Record ID for the Device Locator SDR (Most Significant Byte - MS)

        *res_len = 3;
        *completion_code = 0; // OK
    }

    else
    {
        *res_len = 0;
        *completion_code = 0xCC; // Invalid data field in Request
    }
}

/**
 * @brief Specific function to provide a response for "Set FRU Activation" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.                        
 * @param response_bytes  Data bytes returned by the this function (definition in PICMG v3.0). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 * 
 * This command is used by shelf manager to set the activation or deactivation of initial systems in the FRU.
 * It is also responsible for Hot Swap State transition M2-M3 and M5-M6. Due to this characteristic, the response of the command is integrated
 * with the transitions of the State Machine (FSM) that send the event message.
 */
void ipmi_cmd_set_fru_activation( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    fru_transition_t aux;

    if(request_bytes[0] == 0x00) // Check PICMG Identifier
    {
        // Wait for resources to be created
        while ( queue_fru_transitions == NULL )
            vTaskDelay( pdMS_TO_TICKS(100) );

        if (request_bytes[2] == 0x01) // ShM MSG: ACTIVATE FRU
        {
            aux=SET_FRU_ACTIVATION; 
            xQueueSendToBack(queue_fru_transitions, &aux, portMAX_DELAY); // Send activation to FSM
        }

        if (request_bytes[2] == 0x00) // ShM MSG: DEACTIVATE FRU
        {
            aux=SET_FRU_DEACTIVATION;
            xQueueSendToBack(queue_fru_transitions, &aux, portMAX_DELAY); // Send activation to FSM
        }

        response_bytes[0] = 0x00; // PICMG Identifier
        *res_len = 1;
        *completion_code = 0; // OK
    }

    else
    {
        *res_len = 0;
        *completion_code = 0xCC; // Invalid data field in Request
    }

}

/**
 * @brief Specific function to provide a response for "Compute Power Properties" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.                        
 * @param response_bytes  Data bytes returned by the this function (definition in PICMG v3.0). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_compute_power_prop( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    if(request_bytes[0] == 0x00) // Check PICMG Identifier
    {
        response_bytes[0] = 0x00; // PICMG Identifier
        response_bytes[1] = 0x01; // Single slot Board
        response_bytes[2] = 0x00; // IPMC Location in multi-slot board (not used)

        *res_len = 3;
        *completion_code = 0; // OK
    }

    else
    {
        *res_len = 0;
        *completion_code = 0xCC; // Invalid data field in Request
    }
}

/**
 * @brief Specific function to provide a response for "Get Power Level" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.                        
 * @param response_bytes  Data bytes returned by the this function (definition in PICMG v3.0). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 * 
 * @sa {@link power_manager.h}
 */
void ipmi_cmd_get_power_level( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{

	int i;
	power_envelope_t power_envelope;
	uint8_t          current_power_level;
	uint8_t          desired_power_level;
	
	if(request_bytes[0] == 0x00) //PICMG ID
	{
		// Get the power properties from the power manager
		ipmc_pwr_get_power_properties(&power_envelope, &current_power_level, &desired_power_level);
		
		response_bytes[0] = 0x00; // PICMG Identifier
		
		switch (request_bytes[2])
		{
			case 0x00: // Steady state power draw levels
				response_bytes[1] = current_power_level & 0x1F; // Dynamic power config. and Power level from FRU
			break;

			case 0x01: // Desired steady state power draw levels
				response_bytes[1] = desired_power_level & 0x1F; // Dynamic power config. and Power level from FRU
			break;

			case 0x02: // Early power draw levels. Not implemented.
			case 0x03: // Desired early levels.    Not implemented.
				response_bytes[1] = 0x00; // Dynamic power config. and Power level from FRU
			break;
			
			// Other values are reserved
			default:
				*res_len = 0;
				*completion_code = 0xCC;  // Invalid data field in Request
				// ((void*)0); //This is a NO-OP
				return;

		}
        
		response_bytes[2] = 0x00; // Delay to stable power. Early type not implemented.
		response_bytes[3] = power_envelope.multiplier; // Power Multiplier
		
		// Insert the Power Draw list
		for (i=1; i<=power_envelope.num_of_levels; i++)
			response_bytes[3+i] = power_envelope.power_draw[i];
		
		*res_len = 4 + power_envelope.num_of_levels;
		*completion_code = 0;     // OK
    }

    else
    {
        *res_len = 0;
        *completion_code = 0xCC;  // Invalid data field in Request
    }
}

/**
 * @brief Specific function to provide a response for "Set Power Level" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.                        
 * @param response_bytes  Data bytes returned by the this function (definition in PICMG v3.0). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 * 
 * This command is used by shelf manager to set the power levels of the payload.
 * As the payload is active in the FRU, the FSM send the transition event M3-M4.
 * State M4 is the normal operation of the board.
 * 
 * @sa {@link power_manager.h}
 */
void ipmi_cmd_set_power_level( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    
    if(request_bytes[0] == 0x00) // Check PICMG Identifier
    {
        // Wait for resources to be created
        while ( queue_fru_transitions == NULL )
            vTaskDelay( pdMS_TO_TICKS(100) );

        if ( (request_bytes[2] != 0xff) ) // 0xFF = "Don't change the Power Level". Otherwise is 0 up to 20
        {
            ipmc_pwr_set_power_level_authorized_by_shmc(request_bytes[2]);
        }

        response_bytes[0] = 0x00; // PICMG Identifier
        *res_len = 1;
        *completion_code = 0;     // OK
    }

    else
    {

        *res_len = 0;
        *completion_code = 0xCC;  // Invalid data field in Request
    }
}

/**
 * @brief Specific function to provide a response for "Set Port State" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.                        
 * @param response_bytes  Data bytes returned by the this function (definition in PICMG v3.0). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_set_port_state( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    if(request_bytes[0] == 0x00) // Check PICMG Identifier
    {
        *res_len = 0;
        *completion_code = 0xC1; // NOT SUPPORTED COMMAND
    }

    else
    {
        *res_len = 0;
        *completion_code = 0xCC; // Invalid data field in Request
    }
}

/**
 * @brief Specific function to provide a response for "Get Address Info" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.                        
 * @param response_bytes  Data bytes returned by the this function (definition in PICMG v3.0). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_get_address_info( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    if(request_bytes[0] == 0x00) // Check PICMG Identifier
    {
        response_bytes[0] = 0x00; // PICMG Identifier
        
        response_bytes[1] = ipmb_0_addr >> 1; // Hardware address of the IPMC
        
        response_bytes[2] = ipmb_0_addr; // IPMB address for ipmb-0 of the IPMC
        
        response_bytes[3] = 0xff; // Reserved
        
        response_bytes[4] = 0x00; // FRU Device ID associated with the Site Number
        
        response_bytes[5] = 0x00; // Site Number (not used in ATCA)
        
        response_bytes[6] = 0x00; // Site Type: Front Board
        
        response_bytes[7] = 0xff; // Reserved
        
        response_bytes[8] = 0xff; // Address on IPMI Channel 7 (not used)

        *res_len = 9;
        *completion_code = 0x00; // OK
    }

    else
    {
	  *res_len = 0;
	  *completion_code = 0xCC;  // Invalid data field in Request
    }
}

/**
 * @brief Specific function to provide a response for "Get Shelf Address Info" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.                        
 * @param response_bytes  Data bytes returned by the this function (definition in PICMG v3.0). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_get_shelf_address_info( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    if(request_bytes[0] == 0x00) // Check PICMG Identifier
    {
        response_bytes[0] = 0x00; // PICMG Identifier
        response_bytes[1] = shelf_address_type;
        response_bytes[2] = shelf_address;
        
        *res_len = 3;
        *completion_code = 0x00; // OK
    }

    else
    {
        *res_len = 0;
        *completion_code = 0xCC;  // Invalid data field in Request
    }
}

/**
 * @brief Specific function to provide a response for "Set Shelf Address Info" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.                        
 * @param response_bytes  Data bytes returned by the this function (definition in PICMG v3.0). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_set_shelf_address_info( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    if(request_bytes[0] == 0x00) // Check PICMG Identifier
    {
        response_bytes[0] = 0x00; // PICMG Identifier

        if (request_bytes[1] == 0x00) shelf_address_type = 0x00; // Un-configure shelf address type
        else shelf_address_type = request_bytes[1]; // Configure shelf address type

        shelf_address = request_bytes[2]; // Set shelf address

        *res_len = 1;
        *completion_code = 0x00; // OK
    }

    else
    {
        *res_len = 0;
        *completion_code = 0xCC; // Invalid data field in Request
    }
}

/**
 * @brief Specific function to provide a response for "FRU Control" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.                        
 * @param response_bytes  Data bytes returned by the this function (definition in PICMG v3.0). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 * 
 * Using Shelf Manager interface is possible to send reset commands to FRU. Command response must be integrated with ios interface in IPMC. 
 */
void ipmi_cmd_fru_control( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    if(request_bytes[0] == 0x00 && request_bytes[1] == 0x00) // Check PICMG Identifier and FRU Device ID
    {
        response_bytes[0] = 0x00; // PICMG Identifier

        // TODO IMPLEMENT FRU CONTROL FUNCTION
        if      (request_bytes[1] == 0x00) // Cold Reset
        {
        	fru_control_issued_cold_reset ();
            *res_len = 1;
            *completion_code = 0x00; // OK
        }
        else if (request_bytes[1] == 0x01) // Warm Reset (optional)
        {}
        else if (request_bytes[1] == 0x02) // Graceful Reboot (optional)
        {}
        else if (request_bytes[1] == 0x03) // Issue Diagnostic Interrupt (optional)
        {}
        
        else
        {
            *res_len = 0;
            *completion_code = 0xCC; // Invalid data field in Request
        }
    }

    else
    {
        *res_len = 0;
        *completion_code = 0xCC; // Invalid data field in Request
    }
}

/**
 * @brief Specific function to provide a response for "FRU Control Capabilities" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.                        
 * @param response_bytes  Data bytes returned by the this function (definition in PICMG v3.0). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_fru_control_capabilities( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    if(request_bytes[0] == 0x00 && request_bytes[1] == 0x00) // Check PICMG Identifier and FRU Device ID
    {
        response_bytes[0] = 0x00; // PICMG Identifier

        response_bytes[1] = get_fru_control_capabilities(); // FRU Capabilities Mask

        *res_len = 2;
        *completion_code = 0x00; // OK
    }

    else
    {
        *res_len = 0;
        *completion_code = 0xCC; // Invalid data field in Request
    }
}

/**
 * @brief Specific function to provide a response for "Get FRU LED Properties" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.                        
 * @param response_bytes  Data bytes returned by the this function (definition in PICMG v3.0). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_get_fru_led_prop( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    if(request_bytes[0] == 0x00 && request_bytes[1] == 0x00) // Check PICMG Identifier and FRU Device ID
    {
        response_bytes[0] = 0x00; // PICMG Identifier
        response_bytes[1] = 0x01; // Inform ShM that BLUE LED can be set
        response_bytes[2] = 0x00; // No application specific LEDs

        *res_len = 3;
        *completion_code = 0x00; // OK
    }

    else
    {
        *res_len = 0;
        *completion_code = 0xCC; // Invalid data field in Request
    }
}

/**
 * @brief Specific function to provide a response for "Get LED Color Capabilities" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.                        
 * @param response_bytes  Data bytes returned by the this function (definition in PICMG v3.0). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_get_led_color_capab( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    if(request_bytes[0] == 0x00 && request_bytes[1] == 0x00 && request_bytes[2] == 0x00) // Check PICMG Identifier, FRU Device ID and BLUE LED ID
    {
        response_bytes[0] = 0x00; // PICMG Identifier
        response_bytes[1] = 0x02; // LED supports blue color
        response_bytes[2] = 0x01; // Default LED Color in Local Control State (set to BLUE)
        response_bytes[3] = 0x01; // LED color in Override State (set to BLUE)
        response_bytes[4] = 0x00; // LED Flags (no setting)

        *res_len = 5;
        *completion_code = 0x00;     // OK
    }

    else
    {
        *res_len = 0;
        *completion_code = 0xCC;  // Invalid data field in Request
    }
}

/**
 * @brief Specific function to provide a response for "Set FRU LED State" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.                        
 * @param response_bytes  Data bytes returned by the this function (definition in PICMG v3.0). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_set_fru_led_state( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    if(request_bytes[0] == 0x00) // Check PICMG Identifier
    {
        response_bytes[0] = 0x00; // PICMG Identifier

        //IMPLEMENT LED PHYSICAL INTERFACE

        *res_len = 1;
        *completion_code = 0x00;     // OK
    }

    else
    {
        *res_len = 0;
        *completion_code = 0xCC;  // Invalid data field in Request
    }
}

/**
 * @brief Specific function to provide a response for "Get FRU LED State" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.                        
 * @param response_bytes  Data bytes returned by the this function (definition in PICMG v3.0). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_get_fru_led_state( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    if(request_bytes[0] == 0x00 && request_bytes[1] == 0x00 && request_bytes[2] == 0x00) // Check PICMG Identifier, FRU Device ID and BLUE LED ID
    {
        response_bytes[0] = 0x00; // PICMG Identifier
        response_bytes[1] = 0x00; // LED States (LED FLags, Override and Lamp Test are disabled)

        // LED STATE is not implemented (std answer: LED off)
        response_bytes[2] = 0x00; // Local Control LED Function
        response_bytes[3] = 0x00; // Local Control On-Duration
    /*
        if      (BLUE_LED_STATE == OFF)
        {
            response_bytes[2] = 0x00; // LED is On
            response_bytes[3] = 0x00;
        }
        else if (BLUE_LED_STATE == ON)
        {
            response_bytes[2] = 0xFF; // LED is Off
            response_bytes[3] = 0x00;
        }
        else if (BLUE_LED_STATE == SHORT_BLINK) // 100ms ON - 900ms OFF
        {
            response_bytes[2] = 0x5A; // Time OFF (specify in tens of ms)
            response_bytes[3] = 0x0A; // Time ON (specify in tens of ms)
        }
        else if (BLUE_LED_STATE == LONG_BLINK) // 900ms ON - 100ms OFF
        {
            response_bytes[2] = 0x0A; // Time OFF (specify in tens of ms)
            response_bytes[3] = 0x5A; // Time ON (specify in tens of ms)
        }
    */
        response_bytes[4] = 0x01; // Local Control Color (set to BLUE)

        *res_len = 5;
        *completion_code = 0x00;     // OK
    }

    else
    {
        *res_len = 0;
        *completion_code = 0xCC;  // Invalid data field in Request
    }
}

/**
 * @brief Specific function to provide a response for "Set FRU Activation Policy" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.                        
 * @param response_bytes  Data bytes returned by the this function (definition in PICMG v3.0). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 * 
 * This function is used to manage the Lock Bit (definition in PICMG v3.0).
 */
void ipmi_cmd_set_fru_activation_policy( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    if(request_bytes[0] == 0x00 && request_bytes[1] == 0x00) //PICMG ID - FRU Device ID
    {
        
        // Change in "Locked" bit
        if( (request_bytes[2] & (1<<0)) != 0 )
        {
            if( (request_bytes[3] & (1<<0)) == 0)
                fru_set_locked_bit(0);
            else
                fru_set_locked_bit(1);
        }
        
        // Change in "Deactivation-Locked" bit
        if( (request_bytes[2] & (1<<1)) != 0 )
        {
            if( (request_bytes[3] & 1<<1) == 0)
                fru_set_deactivation_locked_bit(0);
            else
                fru_set_deactivation_locked_bit(1);
        }
        
        response_bytes[0] = 0x00; // PICMG Identifier = 0x00
        *res_len = 1;
        *completion_code = 0x00;  // OK
        
    }
    else
    {
        *res_len = 0;
        *completion_code = 0xCC;  // Invalid data field in Request
    }

}

/**
 * @brief Specific function to provide a response for "Set FRU Activation Policy" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.                        
 * @param response_bytes  Data bytes returned by the this function (definition in PICMG v3.0). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 * 
 * This function is used to manage the Lock Bit (definition in PICMG v3.0).
 */
void ipmi_cmd_get_fru_activation_policy( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
    if(request_bytes[0] == 0x00 && request_bytes[1] == 0x00) //PICMG ID - FRU Device ID
    {
        
        int locked = (int)fru_get_locked_bit();
        int deact_locked = (int)fru_get_deactivation_locked_bit();
        
        response_bytes[0] = 0x00; // PICMG Identifier = 0x00
        response_bytes[1] = (deact_locked << 1) | (locked << 0); // Inform the values of bits
        *res_len = 2;
        *completion_code = 0x00;  // OK
        
    }
    else
    {
        *res_len = 0;
        *completion_code = 0xCC;  // Invalid data field in Request
    }

}

///@}












