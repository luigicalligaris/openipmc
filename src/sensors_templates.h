/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file sensors_templates.h
 * 
 * @author Bruno Augusto Casu
 * 
 * @brief  Headers for the template functions to create SDR
 *
 */

#ifndef SENSORS_TEMPLATES_H
#define SENSORS_TEMPLATES_H

/**
 * @name Custom functions for SDR creation
 * @{
 */

/**
 * @brief Create the Hot Swap Carrier sensor SDR.
 * 
 * @param id_string String to identify the sensor. Maximum of 16 ASCII characters.
 * 
 * This function creates the record for the Hot Swap Carrier sensor, and fill the SDR with predefined parameters, specific for the carrier sensor type.
 */
void create_hot_swap_carrier_sensor (char* id_string);


/**
 * @brief Create the IPMB-0 sensor SDR.
 *
 * @param id_string String to identify the sensor. Maximum of 16 ASCII characters.
 *
 * This function creates the record for the IPMB-0 Sensor, and fill the SDR with predefined parameters, specific for the carrier sensor type.
 */
void create_ipmb0_sensor (char* id_string);


/**
 * @brief Creates an SDR for a generic analog sensor.
 * 
 * This functions creates a SDR for a generic analog sensor. It also gives a number to the sensor and associates the callback for the "Get Sensor Reading" command.
 * 
 * Characteristics:
 * 
 * - Sensor is declared as _enabled_ by default and does not allow to be disabled
 * - Linear sensor (see conversion formula below)
 * - Does not support events
 * - Does not support hysteresis
 * 
 * @param sensor_type The type of sensor. E.g.: TEMPERATURE, VOLTAGE, CURRENT. See \ref sensor_type "Sensor Type".
 * @param base_unit_type See \ref sensor_unit "Sensor Unit".
 * @param m Conversion parameter __M__. From -512 to 511 (2’s complement, signed, 10 bits).
 * @param b Conversion parameter __B__. From -512 to 511 (2’s complement, signed, 10 bits).
 * @param b_exp Power-of-ten exponent for the magnitude of __B__. From -8 to 7 (2’s complement, signed, 4 bits).
 * @param r_exp Power-of-ten exponent for the magnitude of the result. From -8 to 7 (2’s complement, signed, 4 bits).
 * @param threshold_mask The thresholds supported by this sensor. See \ref threshold_mask "Threshold Mask".
 * @param threshold_list 6 length array containing the raw threshold values in crescent order: Lower Non Recoverable, Lower Critical, Lower Non Critical, Upper Non Critical, Upper Critical and Upper Non Recoverable. Values for unsupported thresholds should be set as 0.
 * @param id_string String to identify the sensor. Maximum of 16 ASCII characters.
 * @param get_sensor_reading_func Pointer to the reading function of the sensor.
 * 
 * __Data Conversion:__ The formula below is used by Shelf Manager to convert raw data into the expected unit, where __x__ is the raw data (unsigned, 8 bits) and __y__ is the converted data.
 * 
 * > y = [ M*x + ( B*10<sup>b_exp</sup> ) ]*10<sup>r_exp</sup>
 * 
 * @note This function creates an {@link sdr_type_01_t} struct filled with the chosen parameters and other predefined ones.
 */
void create_generic_analog_sensor_1 (uint8_t sensor_type, uint8_t base_unit_type, int16_t m, int16_t b, int8_t b_exp, int8_t r_exp , uint8_t threshold_mask, uint8_t* threshold_list, char* id_string, sensor_reading_status_t (*get_sensor_reading_func)(sensor_reading_t*));

///@}
 
#endif // SENSORS_TEMPLATES_H
