/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file fru_inventory_manager.h
 * 
 * @authors Bruno Augusto Casu
 * @authors Andre Muller Cascadan
 * 
 * @brief  Management interface for the FRU Information Inventory creation and access.
 *
 * This header file contains the functions for managing the FRU Information data:
 * 
 * + Create the individual areas of the inventory
 * + Aggregate them in a single single array (the FRU Information itself)
 * + Read the FRU Info inventory by segments.
 *
 * **IMPORTANT:** The current implementation only allows *FRU Device ID = 0*.
 * Mezzanines and RTM are still not supported by OpenIPMC.
 * 
 * For more information about the FRU Info format, see *IPMI - Platform Management FRU Information Storage
 * Definition v1.0*.
 *
 */

#ifndef SRC_OPENIPMC_SRC_FRU_INVENTORY_MANAGER_H_
#define SRC_OPENIPMC_SRC_FRU_INVENTORY_MANAGER_H_

#define ASCII_CODE 0xc0

/**
 * This type is used to allocate the individual fields of the FRU Inventory. It holds the pointer of the data array.
 * 
 * @note After the full Inventory is allocated in the definitive array, the pointer of the struct can be freed.
 * @sa create_fru_inventory()
 * 
 */
typedef uint8_t* fru_inventory_field_t;

/**
 * @{
 * @name FRU Inventory Management Functions
 */

/**
 * @brief Specific function to create the Board Info Area of the FRU Inventory.
 *
 * @param mfg_date_time       Manufacturing date and time (the value must be in minutes since 00:00h January 1th 1996).
 * @param board_manufacturer  User defined string to identify the Board Manufacturer (max of 63 ASCII char).
 * @param board_product_name  User defined string to identify the Product Name (max of 63 ASCII char).
 * @param board_serial_number User defined string to identify the Serial Number of the board (max of 63 ASCII char).
 * @param board_part_number   User defined string to identify the Part Number of the board (max of 63 ASCII char).
 * @param fru_file_id         The FRU File version field is a pre-defined field
 *                            provided as a manufacturing aid for verifying the 
 *                            file that was used during manufacture or field update 
 *                            to load the FRU information. (max of 63 ASCII char).
 * 
 * @return A pointer to the record area, which is allocated by this functions via `pvPortMalloc()`.
 * 
 * This function creates the Board Info Area to be added into the FRU Inventory.
 * The size information of the area is contained int its second byte, in chunks 
 * of 8 (size_in_bytes = 8 x byte[1]).
 */
fru_inventory_field_t create_board_info_field (uint32_t     mfg_date_time,
                                               char * const board_manufacturer,
                                               char * const board_product_name,
                                               char * const board_serial_number,
                                               char * const board_part_number,
                                               char * const fru_file_id);


/**
 * @brief Specific function to create the Product Info Area of the FRU Inventory.
 *
 * @param  manufacturer_name          User defined string to identify the Manufacturer Name (max of 63 ASCII char).
 * @param  product_name               User defined string to identify the Product Name (max of 63 ASCII char).
 * @param  product_part_model_number  User defined string to identify the Product Part/Model Number (max of 63 ASCII char).
 * @param  product_version            User defined string to identify the Product Version (max of 63 ASCII char).
 * @param  product_serial_number      User defined string to identify the Product Serial Number (max of 63 ASCII char).
 * @param  asset_tag                  User defined string to identify the Asset Tag (max of 63 ASCII char).
 * @param  fru_file_id                The FRU File version field is a pre-defined field
 *                                    provided as a manufacturing aid for verifying the 
 *                                    file that was used during manufacture or field update 
 *                                    to load the FRU information. (max of 63 ASCII char).
 * 
 * @return A pointer to the record area, which is allocated by this functions via `pvPortMalloc()`.
 * 
 * This function creates the Product Info Area to be added into the FRU Inventory.
 * The size information of the area is contained int its second byte, in chunks 
 * of 8 (size_in_bytes = 8 x byte[1]).
 */
fru_inventory_field_t create_product_info_field (char * const manufacturer_name,
                                                 char * const product_name,
                                                 char * const product_part_model_number,
                                                 char * const product_version,
                                                 char * const product_serial_number,
                                                 char * const asset_tag,
                                                 char * const fru_file_id);

// Create function for the Multi Record Field (currently empty)
size_t create_multi_record_field (uint8_t multi_record[]);

/**
 * @brief Function to create the FRU Inventory.
 *
 * @param internal_use      Pointer for the generated struct by the {@link create_board_info_field ()} function.  
 * @param chassis_info      Pointer for the generated struct by the create filed function.
 * @param board_info        Pointer for the generated struct by the create filed function.
 * @param product_info      Pointer for the generated struct by the create filed function.
 * @param multi_record      Pointer for the generated struct by the create filed function.
 * @param multi_record_size MultiRecord Area size, in bytes
 * 
 * Main function insert the Inventory fields created separately in the {@link fru_inventory} array, which can be accesses by the get_inventory_data() function
 * to return requested data from the inventory. FRU size, in bytes, is stored in the global {@link fru_inventory_size}.
 */
void create_fru_inventory ( fru_inventory_field_t internal_use,
                            fru_inventory_field_t chassis_info,
                            fru_inventory_field_t board_info,
                            fru_inventory_field_t product_info,
                            fru_inventory_field_t multi_record,
                                         uint16_t multi_record_size );


/**
 * @brief Retrieves the size of a FRU Inventory
 *
 * @param fru_id *FRU Device ID* of the target Field Replaceable Unit
 *
 * @return The size of FRU Inventory in bytes or -1 if FRU Device ID is invalid
 *
 */
int get_fru_inventory_size( uint8_t fru_id );



/**
 * @brief Function to retrieve the FRU Inventory bytes requested by "Get FRU Data" IPMI command.
 * 
 * @param fru_id           *FRU Device ID* of the target Field Replaceable Unit
 * @param offset           Offset byte in the Inventory to start the reading
 * @param length           Number of bytes to be read from the Inventory
 * @param returned_bytes   Buffer to return the requested bytes
 * 
 * @return The number of bytes actually read from the inventory or -1 if FRU Device ID is invalid
 *
 * This function is called by the `ipmi_cmd_read_fru_data()` function to retrieve
 * the requested FRU inventory bytes for the respective FRU.
 */
int get_fru_inventory_data( uint8_t fru_id, uint16_t offset, uint8_t length , uint8_t returned_bytes[] );

///@}

#endif /* SRC_OPENIPMC_SRC_FRU_INVENTORY_MANAGER_H_ */
