/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file fru_state_machine.h
 * 
 * @authors Alison Franca Queiroz da Costa
 * @authors Andre Muller Cascadan
 * 
 * @brief  Management task and functions for the operation of the Field Replaceable Unit (FRU) State Machine.
 *
 * This header file has a task declaration for a security activation and deactivation of the FRU State Machine.
 * 
 * The file also defines the causes of state change in the process of activation and deactivation.
 *
 * Functions to control the locked bit and deactivation locked bit were assigned in this file as well.  
 */

#ifndef FRU_STATE_MACHINE_H
#define FRU_STATE_MACHINE_H

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "sdr_definitions.h"

/**
 * @{
 * @name "State Change" causes
 * 
 */
#define CHANGE_CAUSE_NORMAL                            0x0
#define CHANGE_CAUSE_SET_FRU_ACTIVATION                0x1
#define CHANGE_CAUSE_HANDLE_SWITCH                     0x2
#define CHANGE_CAUSE_PROGRAMMATIC_ACTION               0x3
#define CHANGE_CAUSE_COMMUNIC_LOST_REGAIN              0x4
#define CHANGE_CAUSE_LOCAL_COMMUNIC_LOST_REGAIN        0x5
#define CHANGE_CAUSE_ABRUPT_EXTRACTION                 0x6
#define CHANGE_CAUSE_PROVIDED_INFORMATION              0x7
#define CHANGE_CAUSE_INVALID_HARDWARE_ADDR             0x8
#define CHANGE_CAUSE_UNEXPECTED_DEACTIVATION           0x9
#define CHANGE_CAUSE_POWER_FAILURE                     0xA
#define CHANGE_CAUSE_UNKNOWN                           0xF
///@}


//This queue is used to send the transitions events into the state machine
extern QueueHandle_t queue_fru_transitions;

/**
 * @name "FRU State Machine" states
 * @{
 */
typedef enum {
	
	M0 = 0,
	M1 = 1,
	M2 = 2,
	M3 = 3,
	M4 = 4,
	M5 = 5,
	M6 = 6
	
} fru_state_t;
///@}


/**
 * @name "FRU State Machine" transition events
 * @{
 */
typedef enum 
{
	NORMAL_INSERTION,
	CLOSE_HANDLE,
	OPEN_HANDLE,
	SET_FRU_ACTIVATION,
	SET_FRU_DEACTIVATION,
	SET_POWER_LEVEL_ACTIVATION,
	SET_POWER_LEVEL_DEACTIVATION,
	DEACTIVATE_PAYLOAD,
	PAYLOAD_COLD_RESET_ISSUED
} fru_transition_t;
///@}


// TODO: change "SET_POWER_LEVEL" triggers for "Activation Complete". The same for deactivation


/**
 * @name Activation Policy Control
 * @{
 */
/**
 * @brief Set Locked Bit
 * 
 * @param value Reference to the locked bit value, 0 to False and 1 to True.
 * 
 * This function is responsible to set the locked bit value.
 * 
 */
void fru_set_locked_bit(int value);
/**
 * @brief Set Deactivation Locked Bit
 * 
 * @param value Reference to the deactivation locked bit value, 0 to False and 1 to True.
 * 
 * This function is responsible to set the deactivation locked bit value.
 * 
 */
void fru_set_deactivation_locked_bit(int value);
/**
 * @brief Get Locked Bit
 * 
 * This function is responsible to get the locked bit value.
 * 
 * @return Return the locked bit value.
 * 
 */
int fru_get_locked_bit(void);
/**
 * @brief Get Deactivation Locked Bit
 * 
 * This function is responsible to get the deactivation locked bit value.
 * 
 * @return Return the deactivation locked bit value.
 * 
 */
int fru_get_deactivation_locked_bit(void);
///@}

/**
 * @brief Hot Swap Sensor reading callback
 *
 * This function generates response to the Get Sensor Reading command when it
 * refers to the Hot Swap Sensor.
 *
 */
sensor_reading_status_t fru_hot_swap_sensor_reading(sensor_reading_t* sensor_reading);

#endif
